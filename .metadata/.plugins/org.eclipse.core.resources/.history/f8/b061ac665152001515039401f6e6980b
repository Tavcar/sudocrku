import java.awt.Point;
import java.io.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

/**
 * The class <code>Solver</code> is an implementation of a greedy algorithm to solve the knapsack problem.
 *
 */
public class Solver {
    static int[][] DP ;
    static boolean[] T;
    static  Point[] data;
    static HashMap<Point, Integer> M;
    static int bestSoFar;
    /**
     * The main class
     */
    public static void main(String[] args) {
        try {
            solve(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Read the instance, solve it, and print the solution in the standard output
     */
    public static void solve(String[] args) throws IOException {
        String fileName = null;
        
        // get the temp file name
        for(String arg : args){
            if(arg.startsWith("-file=")){
                fileName = arg.substring(6);
            } 
        }
        if(fileName == null)
            return;
        
        // read the lines out of the file
        List<String> lines = new ArrayList<String>();

        BufferedReader input =  new BufferedReader(new FileReader(fileName));
        try {
            String line = null;
            while (( line = input.readLine()) != null){
                lines.add(line);
            }
        }
        finally {
            input.close();
        }
        
        
        // parse the data in the file
        String[] firstLine = lines.get(0).split("\\s+");
        int items = Integer.parseInt(firstLine[0]);
        int capacity = Integer.parseInt(firstLine[1]);

        data = new Point[items + 1];
        if(items*capacity > 100000000){
            System.out
		    .println("input too large");
//            return;
        }
        for(int i=1; i <= items; i++){
          String line = lines.get(i);
          String[] parts = line.split("\\s+");
          data[i] = new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
        }

        Arrays.sort(data,1, items + 1, new Comparator<Point>() {

	    @Override
	    public int compare(Point o1, Point o2) {
		if((o2.y * o1.x) > (o1.y*o2.x)){
		    return -1;
 		} else  if ((o2.y * o1.x) == (o1.y*o2.x)) {
 		    return 0; 
 		} else {
 		    return 1;
 		}
	    }
	});
        int evalV = 0;
        int evalW = capacity;
        for(int i = 1; i <= items; i++){
            if(data[i].y > evalW){
        	evalV += Math.ceil((double)data[i].x * (double)(evalW / (double)data[i].y));
        	break;
            } else {
        	evalV += data[i].x;
        	evalW -= data[i].y;
            }
        }
//        System.out
//		.println(evalV);
//        for(int i = 1; i < data.length; i++){
//            System.out
//		    .format("%5f- v:%d w:%d\n", (data[i].x/(double)data[i].y), data[i].x, data[i].y);
//        }
//        System.out
//		.println();
        T = new boolean[items+1];
        M = new HashMap<Point, Integer>();
        bestSoFar = 0;
//        int sum = 0;
        double start = System.currentTimeMillis();
        System.out
		.println( knap(items, capacity));
        System.out
		.println("time recursive = " + (System.currentTimeMillis() - start));
//        for(int i = 1; i < T.length; i++){
//            System.out
//		    .print(T[i]?"1 ":"0 ");
//            sum += (T[i]?values[i]:0);
//        }
//        System.out
//		.println("\n" + sum);
        M.clear();
        System.gc();
        
        int[] taken = new int[items + 1];
        DP = new int[capacity+1][items+1];
        start  = System.currentTimeMillis();
        for(int j = 1; j <= items; j++){
            for(int i = 0; i <= capacity; i++){
        	if(data[j].y > i)
        	    DP[i][j] = DP[i][j-1];
        	else 
        	    DP[i][j] = Math.max(DP[i][j - 1], data[j].x + DP[i - data[j].y][j - 1]);
            }
        }
        System.out
     		.println("time itterative = " + (System.currentTimeMillis() - start));
        int value = capacity;
        for(int i = items; i > 0; i--){
            if(M.containsKey(new Point(value,i)) && M.containsKey(new Point(value, i-1))){
        	if(M.get(new Point(value,i)) != M.get(new Point(value, i-1))){
        	    T[i] = true;
        	    value -= data[i].y;
        	}
            } else {
        	System.out
			.println("error");
            }
        }
        value = capacity;
        for(int i = items; i > 0; i--){
            if(DP[value][i] != DP[value][i-1]){
        	// we took the item
        	taken[i] = 1;
        	value -= data[i].y;
            }
        }
        // prepare the solution in the specified output format
        System.out.println(DP[capacity][items]+" 0");
        for(int i=1; i <= items; i++){
            System.out.print(taken[i]+" ");
        }
        System.out.println("");        
    }

    private static int knap(int it, int cap) {
	int ret;
	if(M.containsKey(new Point(it,cap))){
	    ret = M.get(new Point(it,cap));
	} else if(it == 0){
	    return 0;
	}else if (cap >= data[it].y){
	    int dontTakeItem = knap(it-1,cap);
	    int takeTheItem = data[it].x + knap(it-1, cap-data[it].y);
	    ret = Math.max(takeTheItem, dontTakeItem);
	} else{
	    ret = knap(it-1,cap);
	}    
	M.put(new Point(it,cap), ret);
	return ret;
    }
}