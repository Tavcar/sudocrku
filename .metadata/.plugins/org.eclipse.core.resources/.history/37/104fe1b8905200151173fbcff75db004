import java.awt.Point;
import java.io.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

/**
 * The class <code>Solver</code> is an implementation of a greedy algorithm to solve the knapsack problem.
 *
 */
public class Solver {
    static int[][] DP ;
    static boolean[] T;
    static  Point[] data;
    static HashMap<Point, Integer> M;
    static int bestSoFar;
    /**
     * The main class
     */
    public static void main(String[] args) {
        try {
            solve(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Read the instance, solve it, and print the solution in the standard output
     */
    public static void solve(String[] args) throws IOException {
        String fileName = null;
        
        // get the temp file name
        for(String arg : args){
            if(arg.startsWith("-file=")){
                fileName = arg.substring(6);
            } 
        }
        if(fileName == null)
            return;
        
        // read the lines out of the file
        List<String> lines = new ArrayList<String>();

        BufferedReader input =  new BufferedReader(new FileReader(fileName));
        try {
            String line = null;
            while (( line = input.readLine()) != null){
                lines.add(line);
            }
        }
        finally {
            input.close();
        }
        
        
        // parse the data in the file
        String[] firstLine = lines.get(0).split("\\s+");
        int items = Integer.parseInt(firstLine[0]);
        int capacity = Integer.parseInt(firstLine[1]);

        data = new Point[items + 1];
        
        for(int i=1; i <= items; i++){
          String line = lines.get(i);
          String[] parts = line.split("\\s+");
          data[i] = new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
        }

//        Arrays.sort(data,1, items + 1, new Comparator<Point>() {
//
//	    @Override
//	    public int compare(Point o1, Point o2) {
//		if((o2.y * o1.x) > (o1.y*o2.x)){
//		    return -1;
// 		} else  if ((o2.y * o1.x) == (o1.y*o2.x)) {
// 		    return 0; 
// 		} else {
// 		    return 1;
// 		}
//	    }
//	});
        

        double start;

        T = new boolean[items+1];
        M = new HashMap<Point, Integer>();       
        
       start = System.currentTimeMillis();
        int solR =  solveRecursive(capacity, items);    
        System.out
		.format("solution: %d in %f\n", solR, System.currentTimeMillis()-start);
        printSolution();
        
        start = System.currentTimeMillis();
        int solDP =  solveDP(capacity, items);
        System.out
		.format("solution: %d in %f\n", solDP, System.currentTimeMillis() - start);
//        printHashmap(capacity, items);
        
        printSolution();
        

         
        
      printHashmap(capacity, items);

        
        
        
        // prepare the solution in the specified output format
//        System.out.println(DP[capacity][items]+" 0");
               
    }
    
    private static int optimisticEvaluation(int items, int capacity){
        int evalV = 0;
        int evalW = capacity;
        for(int i = 1; i <= items; i++){
            if(data[i].y > evalW){
        	evalV += Math.ceil((double)data[i].x * (double)(evalW / (double)data[i].y));
        	break;
            } else {
        	evalV += data[i].x;
        	evalW -= data[i].y;
            }
        }
        return evalV;
    }
    
    private static void printSolution() {
	for(int i=1; i < T.length; i++){
            System.out.print(T[i]?"1 ":"0 ");
        }
        System.out.println(""); 
    }
    private static int solveDP(int capacity, int items){
	DP = new int[capacity+1][items+1];
        for(int j = 1; j <= items; j++){
            for(int i = 0; i <= capacity; i++){
        	if(data[j].y > i)
        	    DP[i][j] = DP[i][j-1];
        	else 
        	    DP[i][j] = Math.max(DP[i][j - 1], data[j].x + DP[i - data[j].y][j - 1]);
            }
        }
        Arrays.fill(T, false);
        int value = capacity;
        for(int i = items; i > 0; i--){
            if(DP[value][i] != DP[value][i-1]){
        	// we took the item
        	T[i] = true;
        	value -= data[i].y;
            }
        }
        return DP[capacity][items];
    }
    
    
    private static int solveRecursive(int capacity, int items){
	//fill the hashmap
	int value = capacity;
	knap(items, capacity);
	
	
	Arrays.fill(T, false);
	//find the solution
        for(int i = items; i > 0; i--){
            if(M.containsKey(new Point(i, value)) && M.containsKey(new Point(i-1, value)) ){
        	
        	if((!M.get(new Point(i, value)).equals(M.get(new Point(i-1, value))))){
        	    T[i] = true;
        	    value -= data[i].y;
        	}
            } else {
//        	System.out
//			.format("error at %d %d\n", i, value);
            }
        }
        return M.get(new Point(items,capacity));
    }
    
    private static void printHashmap(int capacity, int items) {
        for(int i = 0; i <= capacity; i++){
            for(int j = 0; j <= items; j++){
        	if(M.containsKey(new Point(j,i))){
        	    if(M.get(new Point(j,i)) != DP[i][j])
        		System.out
				.println("ERROR at " + i + " " + j);
//        	    System.out
//			    .format("%5d", M.get(new Point(j,i)));
        	} else {
//        	    System.out.format(" null");
        	}
            }
//            System.out
//		    .println();
        }
    }

    private static int knap(int it, int cap) {
	int ret;
	if(M.containsKey(new Point(it,cap))){
	    return M.get(new Point(it,cap));
	} else if(it == 0){
	    ret = 0;
	}else if (cap >= data[it].y){
	    int dontTakeItem = knap(it-1,cap);
	    int takeTheItem = data[it].x + knap(it-1, cap-data[it].y);
	    ret = Math.max(takeTheItem, dontTakeItem);
	} else{
	    ret = knap(it-1,cap);
	}    
	M.put(new Point(it,cap), ret);
	return ret;
    }
}