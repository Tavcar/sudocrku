import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    private Percolation P;
    private double[] values;
    private double stdev;
    private double mean;
    private double lo, hi;
	   public PercolationStats(int N, int T) {    // perform T independent experiments on an N-by-N grid
	       if(N <= 0 || T <= 0)
		   throw new IllegalArgumentException();
	       values = new double[T];
	     
	       for(int i = 0; i < T; i++){
		     P = new Percolation(N);
		   int counter = 0;
		   while(!P.percolates()){
		       int newi = StdRandom.uniform(1,N+1);
		       int newj = StdRandom.uniform(1,N+1);
		       if(!P.isOpen(newi, newj)){
			   counter++;
			   P.open(newi, newj);
		       }
		   }
//		   System.out
//			.println(counter);
		   values[i] = counter/(double)(N*N);
	       }
	       mean = StdStats.mean(values);
	       stdev = StdStats.stddev(values);
	       hi = mean + ((1.96*stdev)/ Math.sqrt(T));
	       lo = mean - ((1.96*stdev) / Math.sqrt(T));
	   }
	   public double mean() {                     // sample mean of percolation threshold
	       return mean;
	   }
	   public double stddev()  {                  // sample standard deviation of percolation threshold
	       return stdev;
	   }
	   public double confidenceLo(){              // low  endpoint of 95% confidence interval
	       return lo;
	   }
	   public double confidenceHi(){              // high endpoint of 95% confidence interval
	       return hi;
	   }

	   public static void main(String[] args) {
	    PercolationStats PS = new PercolationStats(StdIn.readInt(), StdIn.readInt());
	    StdOut.printf("%f\n%f\n%f,%f\n",PS.mean, PS.stdev,PS.lo,PS.hi);
	}
}
