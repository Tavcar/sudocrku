package test;

import java.awt.image.BufferedImageFilter;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeSet;

public class asdfasd {
    static long memory0 = 0, memory1 = 0,counter = 0;
    Random generator;
    String number;
    static Integer[] Indexes() {
        Integer n = 1;
        Integer max = 6;
        Integer[] count = new Integer[n];
        Random generator = new Random();
        for (int i = 0; i < n; i++) {
            count[i] = generator.nextInt(max);
        }
        return count;
    }

    public static String[] Array_Item()  {
        String[] A = new String[6];
        String[] A_1  = {"Cheese", "Pepperoni", "Black"};  

        String[] A_2 = { "Books", "Pens",  
                         "Pencils","Notebooks"};

        String[] A_3 = { "A", "C", "F", "J" };
        String[] A_4 = { "F", "G", "F", "F" };
        String[] A_5 = { "H", "Pens", "Pencils", "J" }; 
        String[] A_6 = { "L", "L", "PL", "Notebooks" };
        Integer[] count = Indexes();
        for(int i=0;i<count.length;i++){
            // TO DO 
              System.out.print("Test"+i);
        }
        return A;
   }

    public static void main(String[] args)  {
        
        for (String item: Array_Item()){
            System.out.print(item);
        }
    }
    
    static class Parser
    {
       final private int BUFFER_SIZE = 1 << 16;
     
       private DataInputStream din;
       private byte[] buffer;
       private int bufferPointer, bytesRead;
     
       public Parser(InputStream in)
       {
          din = new DataInputStream(in);
          buffer = new byte[BUFFER_SIZE];
          bufferPointer = bytesRead = 0;
       }
     
       public int nextInt() throws Exception
       {
          int ret = 0;
          byte c = read();
          while (c <= ' ') c = read();
          //boolean neg = c == '-';
          //if (neg) c = read();
          do
          {
             ret = ret * 10 + c - '0';
             c = read();
          } while (c > ' ');
          //if (neg) return -ret;
          return ret;
       }
     
       private void fillBuffer() throws Exception
       {
          bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
          if (bytesRead == -1) buffer[0] = -1;
       }
     
       private byte read() throws Exception
       {
          if (bufferPointer == bytesRead) fillBuffer();
          return buffer[bufferPointer++];
       }
    }
     
    static void generatePermutations(int n){
	myPermGenerator(new boolean[n], new int[n], 0);
    }
    static void myPermGenerator(boolean[] c, int[] permutation, int depth){
	if(depth >= c.length){
	    StringBuilder x = new StringBuilder("[");
	    for(int i : permutation)
		x.append(i);
	    x.append("]");
	    System.out
	    .println(x.toString());
	    //  	    processPermutation(permutation.cl);
	    //testing the memory footprint
	    //	    memory0 = Math.max(memory0, Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
	    //preventing optimization
	    // counter += System.currentTimeMillis()%10;
	} else {
	    for(int i = 0; i < c.length; i++){
		if(!c[i]){
		    c[i] = true;
		    permutation[i] = depth;
		    myPermGenerator(c, permutation, depth + 1);
		    c[i] = false;
		    permutation[i] = 0;
		}
	    }
	}
    }
    static void myPermGenerator1(boolean[] c, int[] permutation, int depth){
	if(depth >= c.length){
	    //	    StringBuilder x = new StringBuilder("[");
	    //	    for(int i : permutation)
	    //		x.append(i);
	    //	    x.append("]");
	    //	    System.out
	    //		    .println(x.toString());
	    //	    processPermutation(permutation);
	    //testing the memory footprint
	    //	    memory0 = Math.max(memory0, Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
	    //preventing optimization
	    counter += System.currentTimeMillis()%10;
	} else {
	    for(int i = 0; i < c.length; i++){
		if(!c[i]){
		    c[i] = true;
		    permutation[i] = depth;
		    myPermGenerator(c, permutation, depth + 1);
		    c[i] = false;
		    permutation[i] = 0;
		}
	    }
	}
    }


    static void getPermutations(int size) {

	TreeSet<Integer> origSet = new TreeSet<Integer>();

	for(int i = 0; i < size; i++) {
	    origSet.add(i);
	}

	List<Integer> activePerm = new ArrayList<Integer>(origSet);

	System.out.println(">>> Permutation generation start:\n");

	int permCntr = 0;

	boolean hasMore = true;

	long timeMilliStart = (new Date()).getTime();

	while(hasMore) {

	    permCntr++;
	    //	        System.out.println(permCntr + ". " + activePerm);
	    //	        processPermutation(activePerm);
	    memory1 = Math.max(memory1, Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
	    TreeSet<Integer> activeSet = new TreeSet<Integer>(origSet);

	    for(int i = size; i > 0;) {

		i--;
		Integer elem = activePerm.get(i);

		if(i > 0) {

		    int prevIndx = i - 1;
		    Integer prevElem = activePerm.get(prevIndx);

		    if(prevElem < elem) {

			List<Integer> newPerm = new ArrayList<Integer>();

			for(int j = 0; j < prevIndx; j++) {
			    Integer keeper = activePerm.get(j);
			    activeSet.remove(keeper);
			    newPerm.add(keeper);
			}

			Integer incr = prevElem + 1;

			while(!activeSet.contains(incr)) {
			    incr++;
			}

			activeSet.remove(incr);
			newPerm.add(incr);

			newPerm.addAll(activeSet);

			activePerm = newPerm; 

			i = 0;
		    }
		} else {
		    hasMore = false;
		}
	    }
	}

	long timeMilliEnd = (new Date()).getTime();
	long procMillis = timeMilliEnd - timeMilliStart;
	float procSecs = (float)procMillis / 1000f;

	System.out.println("\n>>> Process time (secs): " + procSecs);
    }
}