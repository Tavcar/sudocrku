package test;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class solverSUdoku {

	private static boolean legal (int num, int ii, int jj) {

		int Urow = (ii/3) * 3;
		int Ucolumn = (jj/3)*3;

		for(int i = 0; i < 9; i++){
			//			System.out.println(i + " " + column);
			if(sudoku[i][jj] == num)
				return false;
			//			System.out.println(row + " " + i);
			if(sudoku[ii][i] == num)
				return false;
			//			System.out.println(Urow  + (i / 3) + " " + (Ucolumn + (i % 3)));
			if(sudoku[Urow  + (i % 3)][Ucolumn + ( i / 3)] == num)
				return false;
		}


		return true;
	}
	private static int[][] sudoku;
	public static void main(String[] args) throws IOException {
		BufferedReader in  = new BufferedReader(new InputStreamReader(System.in));
		sudoku = new int[9][9];
		
		for(int i = 0; i < 9; i++){
			StringTokenizer st = new StringTokenizer(in.readLine());
			for(int j = 0; j < 9; j++){
				sudoku[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 9; j++){
				System.out.print(sudoku[i][j] + " ");
			}
			System.out.println();
		}
		//tmp = sudoku.clone();
		double start = System.currentTimeMillis();
		solve();
		System.out.println(System.currentTimeMillis() - start);
		//sudoku = tmp.clone();
		//double start = System.currentTimeMillis();
		//solveBrute(0, 0);
		//System.out.println(System.currentTimeMillis() - start);
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 9; j++){
				//if(sudoku[i][j] == 0){
				//	V.get(sCnt[i][j]).add(new Point(i, j));
				//}
				System.out.print(sudoku[i][j] + " ");
			}
			System.out.println();
		}
		

	}
	
	private static boolean solveBrute(int i, int j){
		if(j >= 9){
			j = 0; 
			i++;
		}
		if(i >= 9)
			return true;
		
		if(sudoku[i][j] != 0)
			return solveBrute(i, j+1);
		
		for(int a = 1; a <= 9; a++){
			if(legal(a,i,j)){
				sudoku[i][j] = a;
				if(solveBrute(i, j+1))
					return true;
			}
		}
		sudoku[i][j] = 0;
		return false;
	}
	
	private static Point findNext(){

		int cnt = Integer.MAX_VALUE,ii=-1,jj=-1;
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 9; j++){
				if(sudoku[i][j] == 0){
					int localCounter = 0;
					for(int k = 1; k <= 9; k++){
						if(legal(k,i,j)){
							localCounter++;
						}
					}
					if(localCounter<cnt){
						cnt = localCounter;
						ii = i; 
						jj = j;
					}
				}
			}
		}
		if(cnt == 0){
			ii=-2;
			jj=-2;
		}
		return new Point(ii,jj);
	}

	private static boolean solve(){
		Point p = findNext();
		if(p.x == -1)
			return true;
		if(p.x == -2)
			return false;
		for(int i = 1; i <= 9; i++){
			if(legal(i,p.x, p.y)){
				sudoku[p.x][p.y] = i;
				if(solve())
					return true;
				
			}
		}
		sudoku[p.x][p.y] = 0;
			return false;
	}
}
