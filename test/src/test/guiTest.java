package test;




import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class guiTest extends Application {
	Button B1,B2;
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("moj prvi gui");
		
		 B1 = new Button("click me");
		 B1.setPrefWidth(80);
		 B2 = new Button("don't click me");
		 B2.setPrefWidth(80);
		B1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if(B1.getText().equals("click me"))
					B1.setText("good job");
				else
					B1.setText("click me");
			}
		});
		B2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				B2.setText("stop!");				
			}
		});
		
//		StackPane layout = new StackPane();
	
		HBox layout = new HBox();
		layout.setStyle("-fx-background-color: #336699;");
		layout.setPadding(new Insets(15, 12, 15, 12));
		layout.setSpacing(10);
		layout.getChildren().add(B1);
		layout.getChildren().add(B2);
		
		VBox vbox = new VBox();
	    vbox.setPadding(new Insets(10));
	    vbox.setSpacing(8);	
	    
	    BorderPane border = new BorderPane();
	    border.setTop(layout);
	    border.setLeft(vbox);
		Scene scene = new Scene(border, 500, 300);
		primaryStage.setScene(scene);
		
		primaryStage.show();
		
	}



}
