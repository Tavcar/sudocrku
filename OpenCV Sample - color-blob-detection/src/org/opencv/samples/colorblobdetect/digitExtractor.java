package org.opencv.samples.colorblobdetect;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.ml.CvKNearest;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;

public class digitExtractor {
	public boolean trained  = false;
	private String 				 tag = "digitExtractor";
	public static final int			 imageDim = 25;
	private CvKNearest			 knn;
	private Mat                  mSamples,mResponses;
	private String				 path;
	private int[] 				 digitCounter;
	private void initMat(){
		mSamples = new Mat();
		mResponses = new Mat();
		mSamples.convertTo(mSamples, CvType.CV_8UC1);
		mResponses.convertTo(mResponses, CvType.CV_32SC1);
	}

	private void train(){
		if(mSamples.height() == 0 || mResponses.height() == 0)
			return;
		trained = true;
		mSamples.convertTo(mSamples, CvType.CV_32FC1);
//		Log.w(tag, mSamples.width() + " x " + mSamples.height() + " ___ " + mResponses.width() + "x"+mResponses.height());
		knn.train(mSamples, mResponses);
		mSamples.release();
		mResponses.release();
	}

	public int recognize(Mat slika) {
		slika.convertTo(slika, CvType.CV_32FC1);
		Mat res = new Mat();
		try{
			knn.find_nearest(slika.reshape(1,1), 3, res, new Mat(), new Mat());
		} catch(Exception e){
			return 0;
		}
		return (int) res.get(0,0)[0];
	}
	
	public void add(Mat sample, int response) {
		if(mSamples == null || mResponses == null){
			initMat();
		}
//		Bitmap S = Bitmap.createBitmap(sample.cols(), sample.rows(), Bitmap.Config.ARGB_8888);
//		Utils.matToBitmap(sample,  S);
		Bitmap S = Bitmap.createBitmap(imageDim,imageDim,Config.ARGB_8888);
		for(int h = 0; h < imageDim; h++){
			for(int w = 0; w < imageDim; w++){
				S.setPixel(h, w, sample.get(h, w)[0] == 255 ? Color.WHITE : Color.BLACK);
			}
		}


		File file = new File(path + File.separator + String.valueOf(response) + File.separator + String.valueOf(digitCounter[response]++) + ".png" ); // the File to save to
//		Log.w(tag, file.getAbsolutePath());
		OutputStream fOut = null;
		try {
			fOut = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.wtf(tag, "file: " + file.toString() + " does not exist");
			e.printStackTrace();
		}

		S.compress(Bitmap.CompressFormat.PNG, 100, fOut); // saving the Bitmap to a file compressed as a PNG without compression rate
		try{
			fOut.flush();
			fOut.close(); // do not forget to close the stream
		} catch( Exception e) {
			Log.e(tag, "function add, IO error");
		}
		pushBack(sample, response);
	}

	private void pushBack(Mat sam, int resp){
	    Mat r = new Mat(1,1,CvType.CV_32SC1);
	    try{
		mSamples.push_back(sam.reshape(1,1));
		r.put(0,0,resp);
		mResponses.push_back(r);
	    } catch(Exception e){
		Log.w(tag, "training error, training image not bw");
	    }
		r.release();
		sam.release();
	}
	public digitExtractor(String path) {

		initMat();
		knn = new CvKNearest();
		this.path = path;
		digitCounter = new int[10];
		for(int i = 1; i < 10; i++){
			File dir = new File(path, String.valueOf(i));
			if(!dir.exists() || !dir.isDirectory()){
				dir.mkdir();
				Log.i(path, "directory " + String.valueOf(i) + " created");
			}

			File files[] = dir.listFiles();
			if(files == null){
				Log.wtf(tag, "file " + dir.toString() + " is not a directory");
			}

			for(File f : files){
				//Log.w(path, f.getAbsolutePath());
				digitCounter[i]++;
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig = Bitmap.Config.ARGB_8888;
				Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), options);
				Mat tmp = new Mat(imageDim, imageDim, CvType.CV_8UC1);
//				Utils.bitmapToMat(bitmap, tmp);
				for(int h = 0; h < bitmap.getHeight(); h++){
					for(int w = 0; w < bitmap.getWidth(); w++){
						tmp.put(h, w, bitmap.getPixel(h, w)==Color.BLACK? 0 : 255);
					}
				}

				pushBack(tmp, i);

				bitmap.recycle();
			}

		}
		train();    	
	}

}
