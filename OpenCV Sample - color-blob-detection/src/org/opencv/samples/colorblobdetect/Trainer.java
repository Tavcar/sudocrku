package org.opencv.samples.colorblobdetect;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Trainer extends Activity {
	private int i,j;
	private ImageView img;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trainer);
		i=0;j=-1;
		img = (ImageView) findViewById(R.id.imageView1);
		showNext();
	}
	
	
	
	private void showNext(){
		do{
			j++;
			if (j >= 9){
				j = 0;
				i++;
			}
			if (i >= 9){
				Intent returnIntent = new Intent();
				setResult(Activity.RESULT_OK,returnIntent);
				finish();
				return;
			}
		} while(MainActivity.S.getImg(i, j) == null);
		Mat numPic = MainActivity.S.getImg(i, j);
		Bitmap bmp = Bitmap.createBitmap(numPic.cols(), numPic.rows(), Bitmap.Config.ARGB_8888);
		Utils.matToBitmap(numPic, bmp);
		bmp = Bitmap.createScaledBitmap(bmp, 100, 100, false);
		img.setImageDrawable(	new BitmapDrawable(getApplicationContext().getResources(), bmp));
	}

	
	
	public void buttonClick(View v){
		char buttonText = ((Button)v).getText().toString().charAt(0);
		if(buttonText != 'X'){
			 MainActivity.S.train(i, j,buttonText-'0');
			 MainActivity.sudoku[j][i] = buttonText-'0';
		}
		showNext();
	}
}
