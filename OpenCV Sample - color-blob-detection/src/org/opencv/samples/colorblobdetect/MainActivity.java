package org.opencv.samples.colorblobdetect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.opencv.core.Mat;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

	private ImageView[] grid;
	public  static int[][] sudoku;
	private Drawable[] images;
	private ToggleButton[] inputButtons;
	private int clicked = -1, imgWidth = 0, screenWidth = 0;
	private static String tag = "dbgTag:";
	private static final int captureRQ = 3452, captureForTrainingRQ = 19, trainRQ = 23;
	private boolean usingTess = true;
	public static gridAnalizer S;
	public static final String TESSBASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SudokuData/";
	static final String DEFAULT_LANGUAGE = "eng";
	static final String TESSDATA_PATH = TESSBASE_PATH + "tessdata/";
	public static double startTimer;

	protected static void passImage(Mat sud, String path) {
		S = new gridAnalizer(sud, path);
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub

		super.onSaveInstanceState(outState);
		for(int i = 0; i < 9; i++){
			outState.putIntArray(String.valueOf(i), sudoku[i]);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initTessData();
		
		Point size = new Point();
		
		getWindowManager().getDefaultDisplay().getSize(size);
		screenWidth = Math.min(size.x, size.y);
		imgWidth = (Math.min(size.x, size.y)-14 )/9;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

			
		initImages();
		
		initGrid();

		initChooser();

		initButtons();

		
		

	}

	private void initTessData(){
		String[] paths = new String[] { TESSBASE_PATH, TESSDATA_PATH };

		for (String p : paths) {
			File dir = new File(p);
			if (!dir.exists()) {
				if (!dir.mkdirs()) {
					Log.w(tag, "ERROR: Creation of directory " + p + " on sdcard failed");
					return;
				} else {
					Log.w(tag, "Created directory " + p + " on sdcard");
				}
			}
		}
		
		if (!(new File(TESSDATA_PATH + DEFAULT_LANGUAGE + ".traineddata")).exists()) {
			try {

				AssetManager assetManager = getAssets();
				InputStream in = assetManager.open("tessdata/" + DEFAULT_LANGUAGE + ".traineddata");
				//GZIPInputStream gin = new GZIPInputStream(in);
				OutputStream out = new FileOutputStream(paths[1] + DEFAULT_LANGUAGE + ".traineddata");

				// Transfer bytes from in to out
				byte[] buf = new byte[1024];
				int len;
				//while ((lenf = gin.read(buff)) > 0) {
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				//gin.close();
				out.close();
				
				Log.w(tag, "Copied " + DEFAULT_LANGUAGE + " traineddata");
			} catch (IOException e) {
				Log.w(tag, "Was unable to copy " + DEFAULT_LANGUAGE + " traineddata " + e.toString());
			}
		}
	}
	
	private  Drawable resize(Drawable image) {
		Bitmap b = ((BitmapDrawable)image).getBitmap();
		Bitmap bitmapResized = Bitmap.createScaledBitmap(b, imgWidth, imgWidth , false);
		return new BitmapDrawable(getResources(), bitmapResized);
	}

	protected void onRestoreInstanceState(Bundle state) {
		super.onRestoreInstanceState(state);
		if(state != null){
			for(int i = 0; i < 9; i++){
				sudoku[i] = state.getIntArray(String.valueOf(i));
			}
			printSudoku();
		}
	}


	private class sudokuSolve extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {
			String response = "";
			if(solve(0, 0)){
				response = "OK";
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if(result.equals("OK")){
			    Log.v(tag, "measurment algorithm " + String.valueOf(System.currentTimeMillis() - startTimer) + "ms" );
				printSudoku();	
			} else {
				Toast.makeText(getApplicationContext(), "no solution", Toast.LENGTH_LONG).show();
			}

		}
	}


	private boolean solve (int x, int y) {
		if (y >= 9){
			y = 0;
			x++;
		}

		if (x >= 9) {
			return true;
		}

		if (sudoku[x][y] != 0) {
			return solve(x,y+1);
		}

		for (int i = 1; i <= 9; i++) {
			if (legal(x, y,i)){
				sudoku[x][y] = i;
				if ( solve(x,y+1)){
					return true;
				} 

			}
		}
		sudoku[x][y] = 0;
		return false;
	}



	public   void printSudoku(){

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				grid[i*9 + j].setImageDrawable(resize(images[sudoku[i][j]]));
			}
		}
	}



	private void cleanSudoku(){
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				//grid[i*9 + j].setImageDrawable(images[0]);
				sudoku[i][j] = 0;
			}
		}		
		printSudoku();
	}
	
	private void initButtons() {
		Button clear = (Button) findViewById(R.id.buttonClear);
		clear.setSaveEnabled(false);
		
		clear.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				cleanSudoku();
				if(S != null)
					S.clean();
			}
		});
		Button solve = (Button) findViewById(R.id.buttonSolve);
		solve.setSaveEnabled(false);
		solve.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sudokuSolve s = new sudokuSolve();
				startTimer = System.currentTimeMillis();
				s.execute();
			}
		});

		Button capture = (Button) findViewById(R.id.buttonCapture);
		capture.setSaveEnabled(false);
		capture.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent cptr = new Intent(v.getContext(), CaptureActivity.class);
				startActivityForResult(cptr, captureRQ);
			}
		});
	}

	@Override 
	public void onActivityResult(int requestCode, int resultCode, Intent data) {     
		super.onActivityResult(requestCode, resultCode, data); 
		if(requestCode == captureRQ) {
			if (resultCode == Activity.RESULT_OK) { 
				if (S != null){
					if(S.isTrained() || usingTess){
						for(int i = 0; i < 9; i++){
							for(int j = 0; j < 9; j++){
								int recognizedDigit = S.get(j, i, usingTess);
								if(recognizedDigit > 0 && recognizedDigit < 10){
									sudoku[i][j] = recognizedDigit;
								}
							}
						}
						printSudoku();
						Log.i("main", "measurment, time " + (System.currentTimeMillis()-startTimer));
					} else {
						startTrainingActivity();
						Toast.makeText(getApplicationContext(), "no train data found", Toast.LENGTH_SHORT).show();
					}
				}
			}
		} else if (requestCode == captureForTrainingRQ){
			if(resultCode == Activity.RESULT_OK){
				startTrainingActivity();
			}
		} else if (requestCode == trainRQ){
			if(resultCode == Activity.RESULT_OK){
				printSudoku();
			}
		}
	}

	private void startTrainingActivity(){
		cleanSudoku();
		Intent train = new Intent(getApplicationContext(), Trainer.class);
		startActivityForResult(train, trainRQ);
	}




	private void initChooser() {
		LinearLayout C = (LinearLayout) findViewById(R.id.inputLayoutOne);
		LinearLayout CC = (LinearLayout) findViewById(R.id.inputLayoutTwo);
		//		Log.i(tag, "layout obtained");
		inputButtons = new ToggleButton[10];
		for (int i = 0; i < inputButtons.length; i++) {
			inputButtons[i] = new ToggleButton(getApplicationContext());
			if(i == 9){
				inputButtons[i].setTextOn("X");
				inputButtons[i].setTextOff("X");
			} else {
				inputButtons[i].setTextOn(String.valueOf(i + 1));
				inputButtons[i].setTextOff(String.valueOf(i+1));
			}
			inputButtons[i].setId((i + 1) * 100);
			inputButtons[i].setChecked(false);
			LinearLayout.LayoutParams pp = new LinearLayout.LayoutParams(screenWidth/5, LinearLayout.LayoutParams.WRAP_CONTENT);
			inputButtons[i].setLayoutParams(pp);
			inputButtons[i].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
					// TODO Auto-generated method stub
					if(arg1) {
						for (int j = 0; j < inputButtons.length; j++) {
							if(arg0.getId() != inputButtons[j].getId() && inputButtons[j].isChecked())
								inputButtons[j].setChecked(false);
						}
						clicked = (arg0.getId()/100);
					}
				}
			});
//			Log.i(tag, "almost done");
			if(i> 4)
				CC.addView(inputButtons[i]);
			else
				C.addView(inputButtons[i]);
		}
	}

	private void initGrid() {
		if (grid != null) return;
		grid = new ImageView[81];
		sudoku = 
				new int[9][9];
		GridLayout table = (GridLayout) findViewById(R.id.board);
		for(int i = 0; i < grid.length; i++) {
			grid[i] = new ImageView(getApplicationContext());
			grid[i].setImageDrawable(resize(images[sudoku[i%9][i/9]]));
			grid[i].setId(i);
			grid[i].setSaveEnabled(false);
			sudoku[i/9][i%9] = 0;
			GridLayout.LayoutParams param = new GridLayout.LayoutParams();
			param.height =imgWidth;
			param.width = imgWidth;
			if (i % 8 == 0) 
				param.rightMargin = 0;
			else if(i % 3 == 2)
				param.rightMargin = 4;
			else 
				param.rightMargin = 1;

			if((i / 9) % 3 ==  0)
				param.topMargin = 4;
			else
				param.topMargin = 1;
			param.setGravity(Gravity.CENTER);
			param.columnSpec = GridLayout.spec(i % 9);
			param.rowSpec = GridLayout.spec(i / 9);
			grid[i].setLayoutParams (param);
			table.addView(grid[i]);
			grid[i].setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if(clicked<=0) return;
					if (legal(v.getId(), clicked % 10)){
						grid[v.getId()].setImageDrawable(images[clicked%10]);
						int ii = v.getId()/9;
						int jj = v.getId()%9;
						sudoku[ii][jj] = clicked%10;	
					}else{
						Toast.makeText(getApplicationContext(), "invalid number", Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}

	private boolean legal (int x, int y, int i) {
		return legal(x*9 + y , i);
	}
	private boolean legal (int possition, int num) {
		if (num == 0) return true;
		int column = possition % 9;
		int row = possition / 9;
		int Urow = (row/3) * 3;
		int Ucolumn = (column/3)*3;

		for(int i = 0; i < 9; i++){
			//			System.out.println(i + " " + column);
			if(sudoku[i][column] == num)
				return false;
			//			System.out.println(row + " " + i);
			if(sudoku[row][i] == num)
				return false;
			//			System.out.println(Urow  + (i / 3) + " " + (Ucolumn + (i % 3)));
			if(sudoku[Urow  + (i / 3)][Ucolumn + ( i % 3)] == num)
				return false;
		}


		return true;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

    	getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	    case R.id.action_clear:
		    trimCache();
	        return true;
	    case R.id.action_train:
//	    	Intent captureForTraining = new Intent(getApplicationContext(), ColorBlobDetectionActivity.class);
	    	Intent captureForTraining = new Intent(getApplicationContext(), CaptureActivity.class);
	    	startActivityForResult(captureForTraining, captureForTrainingRQ);
	    	return true;
	    case R.id.toggleTess:
	    	usingTess ^= true;
	    	toggleTitle(item);
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	private void toggleTitle(MenuItem item){
	    if(usingTess){
    		item.setTitle("use k-NN");
    	} else {
    		item.setTitle("use Tesseract");
    	}
	    
	}
	 public  void trimCache() {
	      try {
	         File dir = getApplicationContext().getFilesDir();
	         if (dir != null && dir.isDirectory()) {
	            deleteDir(dir);
	         }
	      } catch (Exception e) {
	         // TODO: handle exception
	      }
	   }

	   public static boolean deleteDir(File dir) {
	      if (dir != null && dir.isDirectory()) {
	         String[] children = dir.list();
	         for (int i = 0; i < children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	               return false;
	            }
	         }
	      }

	      // The directory is now empty so delete it
	      return dir.delete();
	   }
	private void initImages() {
		if (images != null) return;
		images = new Drawable[10];
		images[0] = getResources().getDrawable(R.drawable.none);
		images[1] = getResources().getDrawable(R.drawable.one);
		images[2] = getResources().getDrawable(R.drawable.two);
		images[3] = getResources().getDrawable(R.drawable.three);
		images[4] = getResources().getDrawable(R.drawable.four);
		images[5] = getResources().getDrawable(R.drawable.five);
		images[6] = getResources().getDrawable(R.drawable.six);
		images[7] = getResources().getDrawable(R.drawable.seven);
		images[8] = getResources().getDrawable(R.drawable.eight);
		images[9] = getResources().getDrawable(R.drawable.nine);
	}

}
