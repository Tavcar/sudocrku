package org.opencv.samples.colorblobdetect;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;

public class CaptureActivity extends Activity implements CvCameraViewListener2, OnTouchListener {
    private static final String TAG = "OCVSample::Activity";

    private cameraView 	mOpenCvCameraView;
    private List<Size> 	mResolutionList;
    private MenuItem[] 	mEffectMenuItems;
    private SubMenu 	mColorEffectsMenu;
    private MenuItem[] 	mResolutionMenuItems;
    private SubMenu 	mResolutionMenu;
    public static int 	mSudSize = 900;
    private Mat		endM,mSud;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
	@Override
	public void onManagerConnected(int status) {
	    switch (status) {
	    case LoaderCallbackInterface.SUCCESS:
	    {
		Log.i(TAG, "OpenCV loaded successfully");
		mOpenCvCameraView.enableView();
		mOpenCvCameraView.setOnTouchListener(CaptureActivity.this);
		List<org.opencv.core.Point> dst_pnt = new ArrayList<org.opencv.core.Point>();
		org.opencv.core.Point p4 = new org.opencv.core.Point(0, 0);
		dst_pnt.add(p4);
		org.opencv.core.Point p5 = new org.opencv.core.Point(mSudSize - 1, 0);
		dst_pnt.add(p5);
		org.opencv.core.Point p6 = new org.opencv.core.Point(mSudSize - 1, mSudSize - 1);
		dst_pnt.add(p6);
		org.opencv.core.Point p7 = new org.opencv.core.Point(0, mSudSize - 1);
		dst_pnt.add(p7);
		endM = Converters.vector_Point2f_to_Mat(dst_pnt);
		mSud = new Mat(new org.opencv.core.Size(mSudSize, mSudSize), CvType.CV_8UC1);
	    } break;
	    default:
	    {
		super.onManagerConnected(status);
	    } break;
	    }
	}
    };

    public CaptureActivity() {
	Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	Log.i(TAG, "called onCreate");
	super.onCreate(savedInstanceState);
	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	//		mContext = this;
	setContentView(R.layout.activity_capture);

	mOpenCvCameraView = (cameraView) findViewById(R.id.tutorial3_activity_java_surface_view);

	mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

	mOpenCvCameraView.setCvCameraViewListener(this);


    }

    @Override
    public void onPause()
    {
	super.onPause();
	if (mOpenCvCameraView != null)
	    mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
	super.onResume();
	if (!OpenCVLoader.initDebug()) {
	    Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
	    OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
	} else {
	    Log.d(TAG, "OpenCV library found inside package. Using it!");
	    mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
	}
    }

    public void onDestroy() {
	super.onDestroy();
	if (mOpenCvCameraView != null)
	    mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {

    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
	if (mResolutionList == null) {
	    mResolutionList = mOpenCvCameraView.getResolutionList();
	    for(Size element : mResolutionList){
		if(mOpenCvCameraView.getResolution().height > element.height && element.height > 1000){
		    mOpenCvCameraView.setResolution(element);
		    Log.i(TAG, "resolution set to: " + String.valueOf(element.width) + "x" + String.valueOf(element.height) );
		}
	    }
	}
	return processImage(inputFrame.rgba(), false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	List<String> effects = mOpenCvCameraView.getEffectList();

	if (effects == null) {
	    Log.e(TAG, "Color effects are not supported by device!");
	    return true;
	}

	mColorEffectsMenu = menu.addSubMenu("Color Effect");
	mEffectMenuItems = new MenuItem[effects.size()];

	int idx = 0;
	ListIterator<String> effectItr = effects.listIterator();
	while(effectItr.hasNext()) {
	    String element = effectItr.next();
	    mEffectMenuItems[idx] = mColorEffectsMenu.add(1, idx, Menu.NONE, element);
	    idx++;
	}

	mResolutionMenu = menu.addSubMenu("Resolution");
	mResolutionList = mOpenCvCameraView.getResolutionList();
	mResolutionMenuItems = new MenuItem[mResolutionList.size()];

	ListIterator<Size> resolutionItr = mResolutionList.listIterator();
	idx = 0;

	while(resolutionItr.hasNext()) {
	    Size element = resolutionItr.next();
	    mResolutionMenuItems[idx] = mResolutionMenu.add(2, idx, Menu.NONE,
		    Integer.valueOf(element.width).toString() + "x" + Integer.valueOf(element.height).toString());
	    idx++;
	}


	return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
	Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
	if (item.getGroupId() == 1)
	{
	    mOpenCvCameraView.setEffect((String) item.getTitle());
	    Toast.makeText(this, mOpenCvCameraView.getEffect(), Toast.LENGTH_SHORT).show();
	}
	else if (item.getGroupId() == 2)
	{
	    int id = item.getItemId();
	    Size resolution = mResolutionList.get(id);
	    mOpenCvCameraView.setResolution(resolution);
	    resolution = mOpenCvCameraView.getResolution();
	    String caption = Integer.valueOf(resolution.width).toString() + "x" + Integer.valueOf(resolution.height).toString();
	    Toast.makeText(this, caption, Toast.LENGTH_SHORT).show();
	}

	return true;
    }

    public  void loadPicture(Mat pic){
	Toast.makeText(getApplicationContext(), "please wait", Toast.LENGTH_LONG).show();
	MainActivity.startTimer = System.currentTimeMillis();
	processImage(pic, true);
	//new asyncImageProcess().execute(pic);
	MainActivity.passImage(mSud, getApplicationContext().getFilesDir().getAbsolutePath());
	Intent returnIntent = new Intent();
	setResult(Activity.RESULT_OK,returnIntent);
	finish();
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
	Log.i(TAG,"onTouch event");
	mOpenCvCameraView.takePicture(this);
	return false;
    }


    private Mat processImage(Mat mRgba, boolean capturedImage){

//	if(capturedImage)
//	    saveImage(mRgba, "0original");
	Mat mBw = new Mat(mRgba.height(), mRgba.width(), CvType.CV_8UC1);
	Imgproc.cvtColor(mRgba, mBw, Imgproc.COLOR_BGR2GRAY);
//	if(capturedImage)
//	    saveImage(mBw, "1gray");
	Imgproc.GaussianBlur(mBw, mBw, new org.opencv.core.Size(5, 5), 0);
//	if(capturedImage)
//	    saveImage(mBw, "2blured");
	Imgproc.adaptiveThreshold(mBw, mBw, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 11, 2);
//	if(capturedImage)
//	    saveImage(mBw, "3treshold");

	List<MatOfPoint> cntrs = new ArrayList<MatOfPoint>();

	Mat H = new Mat();
	Mat contureM = mBw.clone();
	Imgproc.findContours(contureM, cntrs, H, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
//	if(capturedImage)
//	    saveImage(contureM, "3,1countours");
	MatOfPoint2f curveF = new MatOfPoint2f();
	MatOfPoint2f biggest = new MatOfPoint2f();
	double maxArea = 0.0;
	int maxInd = 0;
	MatOfPoint2f approx = new MatOfPoint2f();

	for(int i = 0; i < cntrs.size(); i++){
	    double area = Imgproc.contourArea(cntrs.get(i));
	    curveF = new MatOfPoint2f(cntrs.get(i).toArray());
	    approx = new MatOfPoint2f();
	    double eps = Imgproc.arcLength(curveF, true);
	    Imgproc.approxPolyDP(curveF, approx, 0.02*eps, true);
	    if(area > maxArea && approx.size().height == 4 ){
		maxArea = area;
		biggest = approx;
		maxInd = i;
	    }
	}
	
//	if(capturedImage){
//	    Imgproc.drawContours(mRgba, cntrs, -1, new Scalar (255,0,0));
//	    saveImage(mRgba, "31foundCountours");
//	}

	if(biggest.toArray().length == 4){
	    if(capturedImage){
		Mat pt = Imgproc.getPerspectiveTransform(sort(biggest), endM);
		Imgproc.warpPerspective(mBw, mSud, pt, new org.opencv.core.Size(mSudSize,mSudSize));
//		saveImage(mSud, "4perspectiveTransform");
	    } else {
		Imgproc.drawContours(mRgba, cntrs, maxInd, new Scalar(255,0,0));
		ArrayList<MatOfPoint> tmpc = new ArrayList<MatOfPoint>();
		tmpc.add(new MatOfPoint(biggest.toArray()));
		Imgproc.drawContours(mRgba, tmpc, -1, new Scalar(0,255,0));
	    }
	}
	mBw.release();
	H.release();
	contureM.release();
	curveF.release();
	biggest.release();
	approx.release();
	return mRgba;
    }

    private void saveImage(Mat img, String name){
	Bitmap bmp = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
	Utils.matToBitmap(img, bmp);
	String path = getApplicationContext().getFilesDir().getAbsolutePath();
	File file = new File(path + File.separator + name + ".png" ); // the File to save to

	OutputStream fOut = null;
	try {
		fOut = new FileOutputStream(file);
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}

	bmp.compress(Bitmap.CompressFormat.PNG, 100, fOut); // saving the Bitmap to a file compressed as a PNG without compression rate
	try{
		fOut.flush();
		fOut.close(); // do not forget to close the stream
	} catch( Exception e) {
		e.printStackTrace();
	}
    }
    private MatOfPoint2f sort (MatOfPoint2f A){

	org.opencv.core.Point B[] = A.toArray();
	org.opencv.core.Point out[] = A.toArray();
	if(B.length != 4)
	    return new MatOfPoint2f(out);

	// lower left
	double cmp = Double.MAX_VALUE;
	for(int i = 0; i < B.length; i++){
	    if(cmp > B[i].x + B[i].y){
		cmp = B[i].x + B[i].y;
		out[0] = B[i];
	    }
	}
	// upper right
	cmp = Double.MIN_VALUE;
	for(int i = 0; i < B.length; i++){
	    if(cmp < B[i].x + B[i].y){
		cmp = B[i].x + B[i].y;
		out[2] = B[i];
	    }
	}
	//upper left
	cmp = Double.MAX_VALUE;
	for(int i = 0; i < B.length; i++){
	    if(cmp > B[i].y - B[i].x){
		cmp = B[i].y - B[i].x;
		out[1] = B[i];
	    }
	}
	//lower right
	cmp = Double.MIN_VALUE;
	for(int i = 0; i < B.length; i++){
	    if(cmp < B[i].y - B[i].x){
		cmp = B[i].y - B[i].x;
		out[3] = B[i];
	    }
	}

	return new MatOfPoint2f(out);
    }
}