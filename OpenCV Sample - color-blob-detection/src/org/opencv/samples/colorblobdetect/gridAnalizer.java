package org.opencv.samples.colorblobdetect;


import java.util.ArrayList;
import java.util.List;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import com.googlecode.tesseract.android.TessBaseAPI;

import android.graphics.Bitmap;
import android.util.Log;


public class gridAnalizer {
	private final int  digitSize = CaptureActivity.mSudSize/9;
	private digitExtractor ocr;
	private int[] 		   cntrs;
	private Mat[][]        imgGrid;//, imgGridG;
	private final TessBaseAPI T;






	public gridAnalizer(Mat sudoku, String path){		
		ocr = new digitExtractor(path);
		cntrs   = new int[9];
		imgGrid = new Mat[9][9];
		T = new TessBaseAPI();
		
		T.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_CHAR);
		T.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "123456789");
		T.init(MainActivity.TESSBASE_PATH, MainActivity.DEFAULT_LANGUAGE);
		for(int i = 0; i < 9; i++){
			cntrs[i] = (int)( sudoku.width() * (((2.0*i) + 1.0) / 18.0));
			//Log.w(tag, String.valueOf(cntrs[i]));
		}
		
		

		for(int i = 0; i < 9; i++){
		    for(int j = 0; j < 9; j++){
			Log.i("CC", String.format("extracting: %d %d", i,j));
			extract(sudoku.submat(i*digitSize, (i+1)*digitSize, j*digitSize, (j+1)*digitSize), i, j);
		    }
		}		
	}

	
	public void clean(){
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 9; j++){
				if(imgGrid[i][j] != null)
					imgGrid[i][j].release();
			}
		}
	}
	public Mat getImg(int i, int j){
		if(i < 0 || j < 0 || i > 8 || j > 8 || imgGrid[i][j] == null)
			return null;
		return imgGrid[i][j];
	}

	public boolean isTrained(){
		return ocr.trained;
	}
	public int get(int i, int j, boolean tess){
		if(imgGrid[i][j] == null)
			return 0;
		
		if(tess){
			Bitmap bmp = Bitmap.createBitmap(imgGrid[i][j].cols(), imgGrid[i][j].rows(), Bitmap.Config.ARGB_8888);
			Utils.matToBitmap(imgGrid[i][j], bmp);
			T.setImage(bmp);
			String txt = T.getUTF8Text();
			//Log.w("recognized text", txt);
			if(txt.length() > 0 && txt.charAt(0) > '0' && txt.charAt(0) <= '9')
				return (txt.charAt(0) - '0');
			else
				return 0;
		} else {
			Mat cifra = new Mat();
			Imgproc.resize(imgGrid[i][j], cifra, new Size(digitExtractor.imageDim, digitExtractor.imageDim));
			return ocr.recognize(cifra);
		}
	}

	public void train(int i, int j, int val){
		if(imgGrid[i][j] == null)
			return;
		//img exists
		Mat cifra = new Mat();
		Imgproc.resize(imgGrid[i][j], cifra, new Size(digitExtractor.imageDim, digitExtractor.imageDim));
		ocr.add(cifra, val);
	}

	
	private void extract(Mat digit, int i, int j){
	    Mat tmp = digit.clone();
		Mat H = new Mat();
		List<MatOfPoint> contures = new ArrayList<MatOfPoint>();
		Imgproc.findContours(tmp, contures, H, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
		// filter contoures		
		for(int ci = 0; ci < contures.size(); ci++){
//		    if (H.get(0, ci)[3] >= 0) continue;
			Rect R = Imgproc.boundingRect(contures.get(ci));
			Log.v("CC", String.format("area: %f, hxw: %dx%d", R.area(),R.height,R.width));
			if(R.width < (0.8* digitSize) && R.height < (0.8*digitSize) &&   R.area() > (0.14 * (digitSize*digitSize))){
			    	if(imgGrid[j][i] == null)
			    	    imgGrid[j][i] = digit.submat(R).clone();
			    	else if(imgGrid[j][i].height()*imgGrid[j][i].width() > R.area()){
			    	    imgGrid[j][i] = digit.submat(R).clone();
			    	    Log.w("CC", String.format("REPLACED: %f, hxw: %dx%d", R.area(),R.height,R.width));
			    	}
			    	
				Log.v("CC", String.format("accepted: %f, hxw: %dx%d", R.area(),R.height,R.width));
			}
		}		
	}
}
