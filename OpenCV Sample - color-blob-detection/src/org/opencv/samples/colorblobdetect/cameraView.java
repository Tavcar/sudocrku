package org.opencv.samples.colorblobdetect;


import java.util.List;

import org.opencv.android.JavaCameraView;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.util.Log;

public class cameraView extends JavaCameraView implements PictureCallback {

    private static final String TAG = "Sample::Tutorial3View";
    public cameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public List<String> getEffectList() {
        return mCamera.getParameters().getSupportedColorEffects();
    }

    public boolean isEffectSupported() {
        return (mCamera.getParameters().getColorEffect() != null);
    }

    public String getEffect() {
        return mCamera.getParameters().getColorEffect();
    }

    public void setEffect(String effect) {
        Camera.Parameters params = mCamera.getParameters();
        params.setColorEffect(effect);
        mCamera.setParameters(params);
    }

    public List<Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPictureSizes();
    }

    public void setResolution(Size resolution) {
        Camera.Parameters params = mCamera.getParameters();
        params.setPictureSize(resolution.width, resolution.height);
        mCamera.setParameters(params);
    }

    public Size getResolution() {
        return mCamera.getParameters().getPictureSize();
    }

    
    private CaptureActivity capAct;
    public void takePicture(CaptureActivity caller) {
        Log.i(TAG, "Taking picture");
        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
        mCamera.setPreviewCallback(null);

        // PictureCallback is implemented by the current class
        mCamera.takePicture(null, null, this);
        capAct = caller;
    }
    
 

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.i(TAG, "Saving a bitmap to file");
        // The camera preview was automatically stopped. Start it again.
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);

        // Write the image in a file (in jpeg format)
        try {
        	Bitmap bmp = BitmapFactory.decodeByteArray(data , 0, data.length);
        	Mat picture = new Mat(bmp.getHeight(),bmp.getWidth(),CvType.CV_8UC3);
        	Bitmap myBitmap32 = bmp.copy(Bitmap.Config.ARGB_8888, true);
        	Utils.bitmapToMat(myBitmap32, picture);
        	capAct.loadPicture(picture);
        	Log.w(TAG, String.valueOf(bmp.getWidth()) + "x" + String.valueOf(bmp.getHeight()));
        } catch (Exception e) {
            Log.e("PictureDemo", "Exception in photoCallback", e);
        }

    }
}